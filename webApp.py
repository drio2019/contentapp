#!/usr/bin/python
import socket


class WebApp:

    def parse(self, request):
        parsed_request = request.split(" ")[1]
        return parsed_request

    def process(self, analyzed):
        code = "200 OK"
        bodyHTML = "<html><body><h1>Hello World!</h1></body></html>"
        return code, bodyHTML

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(5)
        try:
            while True:
                print("Waiting for connections...")
                (clientSocket, address) = self.socket.accept()
                print(f"Connection from {address} has been established.")
                request = clientSocket.recv(1024).decode('utf-8')
                parsed_request = self.parse(request)
                code, bodyHTML = self.process(parsed_request)
                response = (f"HTTP/1.1 {code}" +
                            f"\r\n\r\n {bodyHTML} " +
                            "\r\n")
                clientSocket.sendall(response.encode('utf-8'))
                clientSocket.close()
        except KeyboardInterrupt:
            print("Closing server...")
            self.socket.close()
