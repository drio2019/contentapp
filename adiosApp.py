#!/usr/bin/python
from webApp import WebApp

class AdiosApp(WebApp):
    def __init__(self, host, port):
        self.resources_dict = {
            '/': '<html><body><h1>Bienvenido: usa /adios para despedirte</h1></body></html>',
            '/adios': '<html><body><h1>Adios mundo cruel</h1></body></html>'
        }
        super().__init__(host, port)

    def process(self, resource):
        if resource in self.resources_dict:
            code = "200 OK"
            bodyHTML = self.resources_dict[resource]
        else:
            code = "404 Not Found"
            bodyHTML = "<html><body><h1>404 Not Found</h1></body></html>"
        return code, bodyHTML

if __name__ == '__main__':
    app = AdiosApp('', 1234)
